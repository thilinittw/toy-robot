<!DOCTYPE html>
<html lang="en">
<head>
	<title>Toy Robot Simulator</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-md-2">
			</div>

			<div class="col-md-8">

				<h1>Toy Robot Simulator</h1>
				
				<div class="form-group">
					<label for="usr">Input:</label>
					<div class="row">
						<div class="col-md-9">
							<input type="text" class="form-control" id="input_data">
						</div>
						<div class="col-md-3">

							<button type="button" class="btn btn-primary" id="send_data">Submit</button>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			//btn click get input data
			$('#send_data').click(function(){
				var command = $.trim($('#input_data').val());
				if (command) { 
					$.ajax({
                        url: "{{url('send-command')}}",
                        type:"POST",
                        data:{_method: 'POST', _token: "{{csrf_token()}}",command:command},
                        success: function(response){
                            alert(response);
                        }
                    });
				} else { 
					alert("Please intert valid command.");
				}
				
			});
		});
	</script>

</body>
</html>