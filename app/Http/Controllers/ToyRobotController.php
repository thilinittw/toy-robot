<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Session;

class ToyRobotController extends Controller
{
	public function pushText(Request $request)
	{
		$this->validate($request, [
            'command'=> 'required',
        ]);
		$command     = strtolower($request->command);
		$response_txt= "";
		//check session value not empty
		if(!empty($request->session())){
			$x    = Session::get('x');
			$y    = Session::get('y');
			$side = Session::get('side');
		} else {
			$x    = 0;
			$y    = 0;
			$side = '';
		}
		
		if(!strcmp(substr($command,0,5),'place')) {
			$sections = explode(" ", $command);
			if ($sections) {
				//check cordinates
				if($sections[1]) {
					$cordinates = explode(",", $sections[1]);
					$error_values = '';
					if (count($cordinates)==3) {
						//check x cordinates correct
						if (is_numeric($cordinates[0])&&$cordinates[0]<=5) { 
							$x = $cordinates[0];
						} else {
							$error_values = 'X dimention value not correct. Please add correct input.';
						}
						//check y cordinates correct
						if (is_numeric($cordinates[1])&&$cordinates[1]<=5) {
							$y = $cordinates[1];
						} else {
							$error_values = 'Y dimention value not correct. Please add correct input.';
						}
						$side = $cordinates[2];
						if (!$error_values) {
							//change robot cordinates
							Session::put('x', $x);
							Session::put('y', $y);
							Session::put('side', $side);
							$response_txt= 'Next command please.';
						} else {
							return $error_values;
						}
						
					}
				}
			}

		} elseif(!strcmp($command,'move')) {
			$error_values = '';
			switch ($side) {
				case 'north':
					if($y+1<=5) {
						$y++;
					} else {
						$error_values = 'This move may cause the robot to fall.';
					}
					break;
				case 'south':
					if($y>0) {
						$y--;
					} else {
						$error_values = 'This move may cause the robot to fall.';
					}
					break;
				case 'east':
					if($x+1<=5) {
						$x++;
					} else {
						$error_values = 'This move may cause the robot to fall.';
					}
					break;
				case 'west':
					if($y>0) {
						$y--;
					} else {
						$error_values = 'This move may cause the robot to fall.';
					}
					break;
				default:
					$error_values = 'No direction to move.';
					break;
			}
			if (!$error_values) {
				//change robot cordinates
				Session::put('x', $x);
				Session::put('y', $y);
				Session::put('side', $side);
				$response_txt= 'Next command please.';
			} else {
				return $error_values;
			}
		} elseif(!strcmp($command,'left')) {
			$error_values ='';
			switch ($side) {
				case 'north':
					$side = 'west';
					break;
				case 'south':
					$side = 'east';
					break;
				case 'east':
					$side = 'north';
					break;
				case 'west':
					$side = 'south';
					break;
				
				default:
					$error_values = 'No direction to move.';
					break;
			}
			if (!$error_values) {
				//change robot cordinates
				Session::put('side', $side);
				$response_txt= 'Next command please.';
			} else {
				return $error_values;
			}
			
		} elseif(!strcmp($command,'right')) {
			$error_values ='';
			switch ($side) {
				case 'north':
					$side = 'east';
					break;
				case 'south':
					$side = 'west';
					break;
				case 'east':
					$side = 'south';
					break;
				case 'west':
					$side = 'north';
					break;
				
				default:
					$error_values = 'No direction to move.';
					break;
			}
			if (!$error_values) {
				//change robot cordinates
				Session::put('side', $side);
				$response_txt= 'Next command please.';
			} else {
				return $error_values;
			}
		} elseif (!strcmp($command,'report')) {
			Session::get('x');
			Session::get('y');
			Session::get('side');
			$response_txt =  "Output : ".Session::get('x').",".Session::get('y').",".Session::get('side');
		} else {
			$response_txt= "You have entered an incorrect command.";
		}
				
		return $response_txt;
	}
		
   


}
