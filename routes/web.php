<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('toyrobot'); });
Route::post('/send-command', 'ToyRobotController@pushText');
Route::post('/file-upload','ToyRobotController@fileupload');
